<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210919084842 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE field_position (id INT AUTO_INCREMENT NOT NULL, position_name VARCHAR(255) NOT NULL, short_name VARCHAR(5) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE league (id INT AUTO_INCREMENT NOT NULL, league_name VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE player (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, birth_date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE player_stats (id INT AUTO_INCREMENT NOT NULL, player_id INT NOT NULL, season_id INT NOT NULL, number_of_game_played INT NOT NULL, time_on_the_field INT NOT NULL, number_of_assist INT NOT NULL, number_of_goals INT NOT NULL, number_of_started_game INT NOT NULL, number_of_red_card INT NOT NULL, number_of_yellow_card INT NOT NULL, UNIQUE INDEX UNIQ_E8351CEC99E6F5DF (player_id), INDEX IDX_E8351CEC4EC001D1 (season_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE season (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team (id INT AUTO_INCREMENT NOT NULL, team_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team_member (id INT AUTO_INCREMENT NOT NULL, player_id INT NOT NULL, team_id INT NOT NULL, season_id INT NOT NULL, field_position_id INT NOT NULL, INDEX IDX_6FFBDA199E6F5DF (player_id), INDEX IDX_6FFBDA1296CD8AE (team_id), INDEX IDX_6FFBDA14EC001D1 (season_id), INDEX IDX_6FFBDA118339D91 (field_position_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE player_stats ADD CONSTRAINT FK_E8351CEC99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE player_stats ADD CONSTRAINT FK_E8351CEC4EC001D1 FOREIGN KEY (season_id) REFERENCES season (id)');
        $this->addSql('ALTER TABLE team_member ADD CONSTRAINT FK_6FFBDA199E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE team_member ADD CONSTRAINT FK_6FFBDA1296CD8AE FOREIGN KEY (team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE team_member ADD CONSTRAINT FK_6FFBDA14EC001D1 FOREIGN KEY (season_id) REFERENCES season (id)');
        $this->addSql('ALTER TABLE team_member ADD CONSTRAINT FK_6FFBDA118339D91 FOREIGN KEY (field_position_id) REFERENCES field_position (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE team_member DROP FOREIGN KEY FK_6FFBDA118339D91');
        $this->addSql('ALTER TABLE player_stats DROP FOREIGN KEY FK_E8351CEC99E6F5DF');
        $this->addSql('ALTER TABLE team_member DROP FOREIGN KEY FK_6FFBDA199E6F5DF');
        $this->addSql('ALTER TABLE player_stats DROP FOREIGN KEY FK_E8351CEC4EC001D1');
        $this->addSql('ALTER TABLE team_member DROP FOREIGN KEY FK_6FFBDA14EC001D1');
        $this->addSql('ALTER TABLE team_member DROP FOREIGN KEY FK_6FFBDA1296CD8AE');
        $this->addSql('DROP TABLE field_position');
        $this->addSql('DROP TABLE league');
        $this->addSql('DROP TABLE player');
        $this->addSql('DROP TABLE player_stats');
        $this->addSql('DROP TABLE season');
        $this->addSql('DROP TABLE team');
        $this->addSql('DROP TABLE team_member');
    }
}
