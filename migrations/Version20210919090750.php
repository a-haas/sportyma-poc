<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210919090750 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE team_stats (id INT AUTO_INCREMENT NOT NULL, team_id INT NOT NULL, season_id INT NOT NULL, number_of_game_played INT NOT NULL, number_of_win INT NOT NULL, number_of_draw INT NOT NULL, number_of_loss INT NOT NULL, number_of_goal INT NOT NULL, number_of_conceded_goal INT NOT NULL, goal_average INT NOT NULL, league_position INT NOT NULL, league_points INT NOT NULL, INDEX IDX_90412EEA296CD8AE (team_id), INDEX IDX_90412EEA4EC001D1 (season_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE team_stats ADD CONSTRAINT FK_90412EEA296CD8AE FOREIGN KEY (team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE team_stats ADD CONSTRAINT FK_90412EEA4EC001D1 FOREIGN KEY (season_id) REFERENCES season (id)');
        $this->addSql('ALTER TABLE player_stats DROP INDEX UNIQ_E8351CEC99E6F5DF, ADD INDEX IDX_E8351CEC99E6F5DF (player_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE team_stats');
        $this->addSql('ALTER TABLE player_stats DROP INDEX IDX_E8351CEC99E6F5DF, ADD UNIQUE INDEX UNIQ_E8351CEC99E6F5DF (player_id)');
    }
}
