<?php

namespace App\Tests\Service;

use App\Service\LeagueComputeRuleService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class LeagueComputeRuleServiceTest extends KernelTestCase
{

    private function loadLeagueComputeRuleService(): LeagueComputeRuleService {
        // (1) boot the Symfony kernel
        self::bootKernel();

        // (2) use static::getContainer() to access the service container
        $container = static::getContainer();

        // (3) run some service & test the result
        return $container->get(LeagueComputeRuleService::class);
    }

    public function testComputeLeaguePoints() {
        $leagueComputeRuleService = $this->loadLeagueComputeRuleService();
        
        // order is win, draw, loss
        $this->assertEquals($leagueComputeRuleService->computeLeaguePoints(5, 4, 2), (5*3 + 4*1 + 2*0));
    }

    public function testComputeMatchTotal() {
        $leagueComputeRuleService = $this->loadLeagueComputeRuleService();
        
        $this->assertEquals($leagueComputeRuleService->computeMatchTotal(5, 4, 2), (5 + 4 + 2));
    }

    public function testComputeGoalAverage() {
        $leagueComputeRuleService = $this->loadLeagueComputeRuleService();
        
        $this->assertEquals($leagueComputeRuleService->computeGoalAverage(10, 20), (10 - 20));
        $this->assertEquals($leagueComputeRuleService->computeGoalAverage(20, 10), (20 - 10));
    }
}
