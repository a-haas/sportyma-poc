/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

let closeSidebar = document.querySelector("#close-sidebar")
let openSidebar = document.querySelector("#open-sidebar")

if(closeSidebar && openSidebar) {
  openSidebar.addEventListener('click', event => {
    ['hidden', 'flex'].map(c => document.querySelector('#sidebar').classList.toggle(c))
    openSidebar.classList.add('hidden')
    closeSidebar.classList.remove('hidden')
  });
  
  closeSidebar.addEventListener('click', event => {
    ['hidden', 'flex'].map(c => document.querySelector('#sidebar').classList.toggle(c))
    closeSidebar.classList.add('hidden')
    openSidebar.classList.remove('hidden')
  }); 
}