<?php

namespace App\Controller;

use App\Entity\Season;
use App\Entity\Team;
use App\Entity\TeamMember;
use App\Entity\TeamStats;
use App\Entity\User;
use App\Form\TeamType;
use App\Repository\FieldPositionRepository;
use App\Repository\SeasonRepository;
use App\Repository\TeamRepository;
use App\Repository\TeamStatsRepository;
use App\Service\LeagueComputeRuleService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Require ROLE_ADMIN for *every* controller method in this class.
 *
 * @IsGranted("ROLE_ADMIN")
*/
class TeamController extends AbstractController
{
    /**
     * @Route("/team/{season}", name="team_list", requirements={"season"="\d{4}"})
     */
    public function index(Security $security, SeasonRepository $seasonRepository, $season=null): Response
    {
        /** @var User */
        $user = $security->getUser();

        /** @var Season[] */
        $seasons = $seasonRepository->findAll();

        /** @var Season */
        $selectedSeason = $seasonRepository->findOneBy(
            $season ? ['referenceYear' => $season] : ['current' => true]
        );

        return $this->render('team/index.html.twig', [
            'teams' =>  $user->getTeams()->filter(function($el) use ($selectedSeason) {
                /** @var Team $el */
                foreach($el->getTeamStats() as $stats) {
                    /** @var TeamStats $stats */
                    if($stats->getSeason()->getReferenceYear() == $selectedSeason->getReferenceYear()) {
                        return true;
                    }
                }
                return false;
            }),
            'seasons' => $seasons,
            'selectedSeason' => $selectedSeason,
        ]);
    }

    /**
     * @Route("/team/{season}/{id}", name="team_detail", 
     *  requirements={"season"="\d{4}", "id"="\d+"})
     */
    public function team(int $id, int $season,
        Security $security, SeasonRepository $seasonRepository,
        TeamRepository $teamRepository, FieldPositionRepository $fieldPositionRepository
    ): Response {

        /** @var User */
        $user = $security->getUser();

        /** @var Season[] */
        $seasons = $seasonRepository->findAll();

        /** @var Season */
        $selectedSeason = $seasonRepository->findOneBy(['referenceYear' => $season]);

        /** @var Team */
        $team = $teamRepository->find($id);

        $stats = $team->getTeamStats()->filter(function($el) use ($season) {
            /** @var TeamStats $el */
            return $el->getSeason()->getReferenceYear() == $season;
        })->first();

        $members = ($team->getTeamMembers()->filter(function($el) use ($season) {
            /** @var TeamMember $el */
            return $el->getSeason()->getReferenceYear() == $season;
        }));

        $fieldPositions = $fieldPositionRepository->findAll();

        return $this->render('team/index.html.twig', [
            'seasonRef' => $season,
            'teams' =>  $user->getTeams()->filter(function($el) use ($selectedSeason) {
                /** @var Team $el */
                foreach($el->getTeamStats() as $stats) {
                    /** @var TeamStats $stats */
                    if($stats->getSeason()->getReferenceYear() == $selectedSeason->getReferenceYear()) {
                        return true;
                    }
                }
                return false;
            }),
            'seasons' => $seasons,
            'team' => $team,
            'stats' => $stats,
            'members' => $members,
            'fieldPositions' => $fieldPositions,
            'selectedSeason' => $selectedSeason
        ]);
    }

    /**
     * @Route("/team/new", name="team_new")
     */
    public function new(
        Request $request, Security $security, LeagueComputeRuleService $leagueComputeRuleService,
        EntityManagerInterface $em, SeasonRepository $seasonRepository
    ): Response {
        /** @var Season */
        $currentSeason = $seasonRepository->findOneBy([
            'current' => true,
        ]);

        $team = new Team();
        $team->setUser($security->getUser());

        // new player stats
        $teamStats = (new TeamStats)
            ->setSeason($currentSeason);

        // persisting $player is required, otherwise it will throw an error (on addPlayerStat)
        $em->persist($teamStats);
        $em->persist($team);
        
        // glue everything together
        $team->addTeamStat($teamStats);

        $form = $this->createForm(TeamType::class, $team);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // compute missing stats
            $teamStats->setNumberOfGamePlayed($leagueComputeRuleService->computeMatchTotal(
                $teamStats->getNumberOfWin(), $teamStats->getNumberOfDraw(), $teamStats->getNumberOfLoss()
            ));

            $teamStats->setLeaguePoints($leagueComputeRuleService->computeLeaguePoints(
                $teamStats->getNumberOfWin(), $teamStats->getNumberOfDraw(), $teamStats->getNumberOfLoss()
            ));

            $teamStats->setGoalAverage($leagueComputeRuleService->computeGoalAverage(
                $teamStats->getNumberOfGoal(), $teamStats->getNumberOfConcededGoal()
            ));

            $em->flush();
            return $this->redirectToRoute('team_list');
        }

        return $this->render('team/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
