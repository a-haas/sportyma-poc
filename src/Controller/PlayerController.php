<?php

namespace App\Controller;

use App\Entity\Player;
use App\Entity\PlayerStats;
use App\Entity\Season;
use App\Entity\TeamMember;
use App\Entity\TeamStats;
use App\Form\PlayerType;
use App\Repository\PlayerRepository;
use App\Repository\SeasonRepository;
use App\Repository\TeamMemberRepository;
use App\Repository\TeamRepository;
use App\Repository\TeamStatsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Require ROLE_ADMIN for *every* controller method in this class.
 *
 * @IsGranted("ROLE_ADMIN")
*/
class PlayerController extends AbstractController
{
    /**
     * @Route("/player/{season}/{team}", name="player_list",
     *  requirements={"season"="\d{4}", "team"="\d+"})
     */
    public function index($season, $team, SeasonRepository $seasonRepository,
        TeamRepository $teamRepository, TeamMemberRepository $teamMemberRepository, 
        TeamStatsRepository $teamStatsRepository
    ): Response {
        /** @var Season[] */
        $seasons = array_map(function($el) {
            /** @var TeamStats $el */
            return $el->getSeason();
        },$teamStatsRepository->findBy([
            'team' => $team,
        ]));

        $season = $seasonRepository->findOneBy(['referenceYear' => $season]);

        /** @var Player[] */
        $players = $teamMemberRepository->findBy([
            'team' => $team,
            'season' => $season,
        ]);

        return $this->render('player/index.html.twig', [
            'players' => $players,
            'seasons' => $seasons,
            'selectedSeason' => $season,
            'team' => $teamRepository->find($team),
        ]);
    }

    /**
     * @Route("/player/{season}/{team}/{id}", name="player_detail",
     *  requirements={"season"="\d{4}", "team"="\d+", "id"="\d+"})
     */
    public function player($season, $team, $id, SeasonRepository $seasonRepository,
        TeamRepository $teamRepository, TeamStatsRepository $teamStatsRepository,
        TeamMemberRepository $teamMemberRepository, PlayerRepository $playerRepository): Response
    {
        /** @var Season[] */
        $seasons = array_map(function($el) {
            /** @var TeamStats $el */
            return $el->getSeason();
        }, $teamStatsRepository->findBy([
            'team' => $team,
        ]));

        /** @var Season */
        $season = $seasonRepository->findOneBy(['referenceYear' => $season]);

        /** @var Player[] */
        $players = $teamMemberRepository->findBy([
            'team' => $team,
            'season' => $season,
        ]);

        /** @var Player */
        $player = $playerRepository->find($id);

        return $this->render('player/index.html.twig', [
            'players' => $players,
            'seasons' => $seasons,
            'selectedSeason' => $season,
            'team' => $teamRepository->find($team),
            'player' => $player,
            'stats' => $player->getPlayerStats()->filter(function($el) use ($season) {
                /** @var PlayerStats $el */
                return $el->getSeason()->getReferenceYear() == $season->getReferenceYear();
            })->first(),
            'memberOfTeam' => $player->getTeamMembers()->filter(function($el) use ($season) {
                /** @var TeamMember $el */
                return $el->getSeason()->getReferenceYear() == $season->getReferenceYear();
            })->first(),
        ]);
    }

    /**
     * @Route("/player/new", name="player_new")
     */
    public function new(Request $request, EntityManagerInterface $em, SeasonRepository $seasonRepository): Response {
        /** @var Season */
        $currentSeason = $seasonRepository->findOneBy([
            'current' => true,
        ]);

        $player = new Player();
        
        // new player stats
        $playerStats = (new PlayerStats)
            ->setSeason($currentSeason);
        
        // new player membership
        $teamMember = (new TeamMember)
            ->setSeason($currentSeason);

        // persisting $player is required, otherwise it will throw an error (on addPlayerStat)
        $em->persist($player);
        $em->persist($playerStats);
        $em->persist($teamMember);
        
        // glue everything together
        $player->addPlayerStat($playerStats)
            ->addTeamMember($teamMember);

        $form = $this->createForm(PlayerType::class, $player);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('player_detail', [
                'season' => $currentSeason->getReferenceYear(),
                'team' => $teamMember->getTeam()->getId(),
                'id' => $player->getId(),
            ]);
        }

        return $this->render('player/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
