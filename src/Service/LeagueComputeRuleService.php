<?php

namespace App\Service;

class LeagueComputeRuleService {
    public function __construct() {
        
    }

    public function computeLeaguePoints(int $win, int $draw, int $loss = 0) {
        return $win * 3 + $draw * 1 + $loss * 0;
    }

    public function computeMatchTotal(int $win, int $draw, int $loss) {
        return $win + $draw + $loss;
    }

    public function computeGoalAverage(int $scored, int $conceded) {
        return $scored - $conceded;
    }
}