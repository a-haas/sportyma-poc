<?php

namespace App\Command;

use App\Entity\FieldPosition;
use App\Entity\League;
use App\Entity\Player;
use App\Entity\PlayerStats;
use App\Entity\Season;
use App\Entity\Team;
use App\Entity\TeamMember;
use App\Entity\TeamStats;
use App\Entity\User;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DbRestoreCommand extends Command
{
    protected static $defaultName = 'app:db:restore';
    protected static $defaultDescription = 'Restore db with PL data';

    /**
     *
     * @var EntityManagerInterface
     */
    private $em;
    
    /**
     *
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(EntityManagerInterface $em, UserRepository $userRepository)
    {
        $this->em = $em;
        $this->userRepository = $userRepository;
        parent::__construct();
    }

    protected function configure(): void {}

    private function fieldPositions() {
        return [
            "GK" => (new FieldPosition)->setPositionName("Goalkeeper")->setShortName("GK"),
            "DEF" => (new FieldPosition)->setPositionName("Defender")->setShortName("DEF"),
            "MID" => (new FieldPosition)->setPositionName("Midfielder")->setShortName("MID"),
            "FWD" => (new FieldPosition)->setPositionName("Forward")->setShortName("FWD"),
        ];
    }

    private function seasons() {
        return [
            '2016-17' => (new Season())
                ->setReferenceYear(2016)
                ->setSeasonLabel("2016 - 2017"),
            '2017-18' => (new Season())
                ->setReferenceYear(2017)
                ->setSeasonLabel("2017 - 2018"),
            '2018-19' => (new Season())
                ->setReferenceYear(2018)
                ->setSeasonLabel("2018 - 2019"),
            '2019-20' => (new Season())
                ->setReferenceYear(2019)
                ->setSeasonLabel("2019 - 2020"),
            '2020-21' => (new Season())
                ->setReferenceYear(2020)
                ->setSeasonLabel("2020 - 2021")
        ];
    }

    private function league() {
        return (new League)->setLeagueName("Premier League")->setCountry("ENG");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var User */
        $user = $this->userRepository->findAll()[0];

        $fieldPositions = $this->fieldPositions();

        foreach($fieldPositions as $fp) {
            $this->em->persist($fp);
        }

        $league = $this->league();
        $this->em->persist($league);
        
        $teams = [];
        
        $seasons = $this->seasons();

        foreach($seasons as $year => $season) {
            $this->em->persist($season);

            $arr = array_map('str_getcsv', file("datasets/$year-teams.csv"));
            // remove first line (= header)
            array_shift($arr);
            foreach($arr as $t) {
                $newTeam = $teams[$t[2]] ?? (new Team)->setTeamName($t[2])->setLeague($league)->setUser($user);
                $teamStats = (new TeamStats)->setTeam($newTeam)
                    ->setSeason($season)
                    ->setNumberOfGamePlayed((int)$t[4])
                    ->setNumberOfWin((int)$t[5])
                    ->setNumberOfDraw((int)$t[6])
                    ->setNumberOfLoss((int)$t[7])
                    ->setNumberOfGoal((int)$t[8])
                    ->setNumberOfConcededGoal((int)$t[9])
                    ->setGoalAverage((int)$t[10])
                    ->setLeaguePoints((int)$t[11])
                    ->setLeaguePosition((int)$t[3]);
                $teams[$newTeam->getTeamName()] = $newTeam->addTeamStat($teamStats);
                $league->addTeam($newTeam);
                $this->em->persist($newTeam);
                $this->em->persist($teamStats);
            }

            $arr = array_map('str_getcsv', file("datasets/$year-players.csv"));
            // remove header line
            array_shift($arr);
            foreach($arr as $p) {
                $firstname = (explode(" ", $p[0], 2))[0];
                $lastname = (explode(" ", $p[0], 2))[1];
                $newPlayer = (new Player)->setFirstname($firstname)
                    ->setLastname($lastname)
                    ->setEmail("test@talent-sparkler.com")
                    ->setBirthDate(new DateTime("1991-01-01"));
                $playerStats = (new PlayerStats)->setPlayer($newPlayer)
                    ->setSeason($season)
                    ->setNumberOfGamePlayed((int) $p[2])
                    ->setNumberOfStartedGame((int) $p[2])
                    ->setTimeOnTheField((int) $p[3])
                    ->setNumberOfGoals((int) $p[4])
                    ->setNumberOfAssist((int) $p[5])
                    ->setNumberOfYellowCard((int) $p[6])
                    ->setNumberOfRedCard((int) $p[7]);
                $newPlayer->addPlayerStat($playerStats);

                $member = (new TeamMember)
                    ->setPlayer($newPlayer)
                    ->setSeason($season)
                    ->setTeam($teams[$p[8]])
                    ->setFieldPosition($fieldPositions[$p[1]]);

                $member->getTeam()->addTeamMember($member);

                $this->em->persist($newPlayer);
                $this->em->persist($playerStats);
                $this->em->persist($member);
            }
        }

        // add at the end to respect the ID ordering
        $currentSeason = (new Season())
            ->setReferenceYear(2021)
            ->setSeasonLabel("2021 - 2022")
            ->setCurrent(true);
        $this->em->persist($currentSeason);

        $this->em->flush();

        $io = new SymfonyStyle($input, $output);
        $io->success('DB has been successfully restored from the database!');

        return Command::SUCCESS;
    }
}
