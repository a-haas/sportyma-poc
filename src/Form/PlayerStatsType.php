<?php

namespace App\Form;

use App\Entity\PlayerStats;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlayerStatsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numberOfGamePlayed')
            ->add('timeOnTheField')
            ->add('numberOfAssist')
            ->add('numberOfGoals')
            ->add('numberOfStartedGame')
            ->add('numberOfRedCard')
            ->add('numberOfYellowCard')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PlayerStats::class,
        ]);
    }
}
