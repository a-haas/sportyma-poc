<?php

namespace App\Form;

use App\Entity\TeamStats;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TeamStatsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numberOfWin', IntegerType::class)
            ->add('numberOfDraw', IntegerType::class)
            ->add('numberOfLoss', IntegerType::class)
            ->add('numberOfGoal', IntegerType::class)
            ->add('numberOfConcededGoal', IntegerType::class)
            ->add('leaguePosition')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TeamStats::class,
        ]);
    }
}
