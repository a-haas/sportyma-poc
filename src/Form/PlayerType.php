<?php

namespace App\Form;

use App\Entity\Player;
use App\Repository\TeamRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlayerType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('birthDate', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('teamMembers', CollectionType::class, [
                'entry_type' => TeamMemberType::class,
            ])
            ->add('playerStats', CollectionType::class, [
                'entry_type' => PlayerStatsType::class,
            ])
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'bg-gray-200 hover:bg-gray-300 h-8 px-4 rounded-md'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Player::class, 
        ]);
    }
}
