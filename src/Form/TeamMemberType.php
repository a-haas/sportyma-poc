<?php

namespace App\Form;

use App\Entity\FieldPosition;
use App\Entity\Season;
use App\Entity\Team;
use App\Entity\TeamMember;
use App\Entity\TeamStats;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TeamMemberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('team', EntityType::class, [
                'class' => Team::class,
                'choice_label' => 'teamName',
                // retrieve teams from the current season
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->join(TeamStats::class, 'ts', 'WITH', 'ts.team = t')
                        ->join(Season::class, 's', 'WITH', 'ts.season = s')
                        ->where('s.current = true')
                        ->orderBy('t.teamName', 'ASC');
                },
            ])
            ->add('fieldPosition', EntityType::class, [
                'class' => FieldPosition::class,
                'choice_label' => 'positionName',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TeamMember::class,
        ]);
    }
}
