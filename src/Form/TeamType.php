<?php

namespace App\Form;

use App\Entity\League;
use App\Entity\Team;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TeamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('teamName', TextType::class)
            ->add('league', EntityType::class, [
                'class' => League::class,
                'choice_label' => 'leagueName',
            ])
            ->add('teamStats', CollectionType::class, [
                'entry_type' => TeamStatsType::class,
            ])
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'bg-gray-200 hover:bg-gray-300 h-8 px-4 rounded-md'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Team::class,
        ]);
    }
}
