<?php

namespace App\Repository;

use App\Entity\PlayerBelonging;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlayerBelonging|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlayerBelonging|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlayerBelonging[]    findAll()
 * @method PlayerBelonging[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayerBelongingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlayerBelonging::class);
    }

    // /**
    //  * @return PlayerBelonging[] Returns an array of PlayerBelonging objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlayerBelonging
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
