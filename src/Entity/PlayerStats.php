<?php

namespace App\Entity;

use App\Repository\PlayerStatsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlayerStatsRepository::class)
 */
class PlayerStats
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfGamePlayed;

    /**
     * @ORM\Column(type="integer")
     */
    private $timeOnTheField;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfAssist;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfGoals;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfStartedGame;

    /**
     * @ORM\ManyToOne(targetEntity=Season::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $season;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfRedCard;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfYellowCard;

    /**
     * @ORM\ManyToOne(targetEntity=Player::class, inversedBy="playerStats")
     * @ORM\JoinColumn(nullable=false)
     */
    private $player;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumberOfGamePlayed(): ?int
    {
        return $this->numberOfGamePlayed;
    }

    public function setNumberOfGamePlayed(int $numberOfGamePlayed): self
    {
        $this->numberOfGamePlayed = $numberOfGamePlayed;

        return $this;
    }

    public function getTimeOnTheField(): ?int
    {
        return $this->timeOnTheField;
    }

    public function setTimeOnTheField(int $timeOnTheField): self
    {
        $this->timeOnTheField = $timeOnTheField;

        return $this;
    }

    public function getNumberOfAssist(): ?int
    {
        return $this->numberOfAssist;
    }

    public function setNumberOfAssist(int $numberOfAssist): self
    {
        $this->numberOfAssist = $numberOfAssist;

        return $this;
    }

    public function getNumberOfGoals(): ?int
    {
        return $this->numberOfGoals;
    }

    public function setNumberOfGoals(int $numberOfGoals): self
    {
        $this->numberOfGoals = $numberOfGoals;

        return $this;
    }

    public function getNumberOfStartedGame(): ?int
    {
        return $this->numberOfStartedGame;
    }

    public function setNumberOfStartedGame(int $numberOfStartedGame): self
    {
        $this->numberOfStartedGame = $numberOfStartedGame;

        return $this;
    }

    public function getSeason(): ?Season
    {
        return $this->season;
    }

    public function setSeason(?Season $season): self
    {
        $this->season = $season;

        return $this;
    }

    public function getNumberOfRedCard(): ?int
    {
        return $this->numberOfRedCard;
    }

    public function setNumberOfRedCard(int $numberOfRedCard): self
    {
        $this->numberOfRedCard = $numberOfRedCard;

        return $this;
    }

    public function getNumberOfYellowCard(): ?int
    {
        return $this->numberOfYellowCard;
    }

    public function setNumberOfYellowCard(int $numberOfYellowCard): self
    {
        $this->numberOfYellowCard = $numberOfYellowCard;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }
}
