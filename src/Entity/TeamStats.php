<?php

namespace App\Entity;

use App\Repository\TeamStatsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TeamStatsRepository::class)
 */
class TeamStats
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Team::class, inversedBy="teamStats")
     * @ORM\JoinColumn(nullable=false)
     */
    private $team;

    /**
     * @ORM\ManyToOne(targetEntity=Season::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $season;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfGamePlayed;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfWin;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfDraw;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfLoss;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfGoal;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfConcededGoal;

    /**
     * @ORM\Column(type="integer")
     */
    private $goalAverage;

    /**
     * @ORM\Column(type="integer")
     */
    private $leaguePosition;

    /**
     * @ORM\Column(type="integer")
     */
    private $leaguePoints;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }

    public function getSeason(): ?Season
    {
        return $this->season;
    }

    public function setSeason(?Season $season): self
    {
        $this->season = $season;

        return $this;
    }

    public function getNumberOfGamePlayed(): ?int
    {
        return $this->numberOfGamePlayed;
    }

    public function setNumberOfGamePlayed(int $numberOfGamePlayed): self
    {
        $this->numberOfGamePlayed = $numberOfGamePlayed;

        return $this;
    }

    public function getNumberOfWin(): ?int
    {
        return $this->numberOfWin;
    }

    public function setNumberOfWin(int $numberOfWin): self
    {
        $this->numberOfWin = $numberOfWin;

        return $this;
    }

    public function getNumberOfDraw(): ?int
    {
        return $this->numberOfDraw;
    }

    public function setNumberOfDraw(int $numberOfDraw): self
    {
        $this->numberOfDraw = $numberOfDraw;

        return $this;
    }

    public function getNumberOfLoss(): ?int
    {
        return $this->numberOfLoss;
    }

    public function setNumberOfLoss(int $numberOfLoss): self
    {
        $this->numberOfLoss = $numberOfLoss;

        return $this;
    }

    public function getNumberOfGoal(): ?int
    {
        return $this->numberOfGoal;
    }

    public function setNumberOfGoal(int $numberOfGoal): self
    {
        $this->numberOfGoal = $numberOfGoal;

        return $this;
    }

    public function getNumberOfConcededGoal(): ?int
    {
        return $this->numberOfConcededGoal;
    }

    public function setNumberOfConcededGoal(int $numberOfConcededGoal): self
    {
        $this->numberOfConcededGoal = $numberOfConcededGoal;

        return $this;
    }

    public function getGoalAverage(): ?int
    {
        return $this->goalAverage;
    }

    public function setGoalAverage(int $goalAverage): self
    {
        $this->goalAverage = $goalAverage;

        return $this;
    }

    public function getLeaguePosition(): ?int
    {
        return $this->leaguePosition;
    }

    public function setLeaguePosition(int $leaguePosition): self
    {
        $this->leaguePosition = $leaguePosition;

        return $this;
    }

    public function getLeaguePoints(): ?int
    {
        return $this->leaguePoints;
    }

    public function setLeaguePoints(int $leaguePoints): self
    {
        $this->leaguePoints = $leaguePoints;

        return $this;
    }
}
