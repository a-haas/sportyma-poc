<?php

namespace App\Entity;

use App\Repository\SeasonRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;

/**
 * @ORM\Entity(repositoryClass=SeasonRepository::class)
 */
class Season
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $seasonLabel;

    /**
     * @ORM\Column(type="integer")
     */
    private $referenceYear;

    /**
     * @ORM\Column(type="boolean")
     */
    private $current = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSeasonLabel(): ?string
    {
        return $this->seasonLabel;
    }

    public function setSeasonLabel(string $seasonLabel): self
    {
        $this->seasonLabel = $seasonLabel;

        return $this;
    }

    public function getReferenceYear(): ?int
    {
        return $this->referenceYear;
    }

    public function setReferenceYear(int $referenceYear): self
    {
        $this->referenceYear = $referenceYear;

        return $this;
    }

    public function getCurrent(): ?bool
    {
        return $this->current;
    }

    public function setCurrent(bool $current): self
    {
        $this->current = $current;

        return $this;
    }
}
