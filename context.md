## Contexte 
Un club de football souhaite digitaliser la gestion de ses joueurs et de leurs statistiques.
Le dirigeant du club souhaite avoir une interface lui permettant de visualiser l’ensemble des équipes de son club 
ainsi que les joueurs associés.

Voici le service tel que proposé:
* **Page de connexion permettant à l’administrateur de se connecter**
* **Liste des équipes**
* **Détails d’une équipe: la liste de ses joueurs pour une saison donnée**
  * Possibilité de changer la saison courante
* **Détails d’un joueur**
  * Statistiques du joueur pour son équipe pour une saison donnée**
    * Nombre de matchs joués
    * Nombre de minutes jouées
    * Nombre de passes décisives
    * Nombre de buts marqués
* **Possibilité de créer une équipe**
  * Nom de l’équipe
  * Niveau de l’équipe
* **Possibilité de créer un joueur et de l’affecter à son équipe**
  * Email
  * Nom
  * Prénom
  * Date de naissance

Points à anticiper: le club voudra peut-être partager son service avec d’autres dirigeants de club, le modèle de données doit être anticipé en ce sens

## Étape 1 : développement du projet

* Présentation détaillée de la structure du projet et des choix techniques effectués
* Développement de la plateforme
* Fixtures et tests techniques / fonctionnels
* Compte rendu 

## Étape 2 : l’évolution du projet

Le lot 1 étant maintenant fonctionnel, le club souhaite poursuivre le développement de sa plateforme en créant un  accès aux joueurs  sur l’interface en ligne pour qu’ils puissent eux-mêmes renseigner leurs statistiques et mettre à jour leur profil.

Le gestionnaire du club souhaite cependant lancer lui-même la création automatique 
des comptes utilisateurs pour chaque équipe, de manière indépendante.

On estime qu’à ce stade, le gestionnaire aura 15 équipes d’environ 30 membres chacune.

* Réalisez une présentation fonctionnelle de l’évolution du service
* Présentez une architecture technique  
* Estimez le temps nécessaire et les étapes nécessaires aux tests / mise en ligne du nouveau service à l’aide d’un planning détaillé

## Critères d’évaluation

* Compréhension et respect des consignes
* Force de proposition
* Pertinence des choix techniques
* Structure globale du projet
* Pertinence et présence des commentaires
* Tests techniques et fonctionnels

## Rendu du sujet 

Partage d’un repository GitHub accompagné d’un email de rendu à job@sportyma.com
Les documents de présentation sont à placer à la racine du projet
Un fichier README détaillant l’ensemble des travaux et des rendus est un plus


NB: le présent sujet ne sera utilisé qu’à des fins d’évaluation dans le cadre de votre candidature

NB2: En cas de question par rapport au présent sujet, vous pouvez également envoyer 
un email à job@sportyma.com en précisant le sujet concerné
