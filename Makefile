serve:
	docker-compose -f docker-setup/docker-compose.yml --env-file .env up

clear:
	docker-compose -f docker-setup/docker-compose.yml --env-file .env run php-fpm php bin/console cache:clear

console:
	docker-compose -f docker-setup/docker-compose.yml --env-file .env run php-fpm php bin/console $(cmd)

restore:
	make console cmd="doctrine:schema:drop --force"
	make console cmd="doctrine:schema:create"
	make console cmd="app:user:create 'Pep' 'Guardiola' 'pep.guardiola@talent-sparkler.com' pep.guardiola"
	make console cmd="app:user:promote 'pep.guardiola@talent-sparkler.com' ROLE_ADMIN"
	make console cmd="app:db:restore"

test:
	docker-compose -f docker-setup/docker-compose.yml --env-file .env run php-fpm php ./vendor/bin/phpunit