# Talent Sparkler

## Start the app

Here are the few steps required to start the app:

1. Restore the database: `make restore`
2. Start the app: `make serve`

By default, the `make restore` command will create this default user with the *ROLE_ADMIN*:

* Name: Pep Guardiola
* Email address: pep.guardiola@talent-sparkler.com
* Password: pep.guardiola

Feel free to change the Makefile if needed, the restore command will associates the created team with the first user created in the DB.

## App utilities

* To clear the cache of the app run `make clear`
* To run a SF console command inside the *php-fpm* docker container use `make console cmd="<your SF command>"` 
* To run the tests use `make test`

## Resources used

### Docker

App is built on docker with Nginx, MariaDB, PHPMyAdmin and PHP-FPM images glued together to have a SF5 app.

The env setup is based on the following:

* [https://dev.to/martinpham/symfony-5-development-with-docker-4hj8]()
* [https://gitlab.com/martinpham/symfony-5-docker]()

### *make restore*

The `make restore` uses a Premier League dataset from the seasons 2016 to 2021 to generate accurate players, stats and teams.

The dataset is hacked from several Fantasy Premier League datasets.

### Design

The design is mostly inspired from the following resources:

* https://lofiui.co/
* https://heroicons.dev/

## Missing parts

Obviously since this is a POC there are a lot of missing parts. Here is a list of a few of them:

* Missing translations
* Missing front-end framework like Vue.js or React.js
* Missing business services classes (and their definitions)
* Advanced testing
* ...

## Installation requirements

* *Docker*
* *Docker compose*
* *Makefile*